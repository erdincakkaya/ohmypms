var express = require('express');
var router = express.Router();

var helper = require('sendgrid').mail;


router.get('/e8d1fe17bc40.html', function(req, res, next) {
  res.render('verify', { title: 'a622e7334050' });
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/download', function (req, res, next) {

  var isMobile = {
    Windows: function() {
        return /IEMobile/i.test(req.headers['user-agent']);
    },
    Android: function() {
        return /Android/i.test(req.headers['user-agent']);
    },
    BlackBerry: function() {
        return /BlackBerry/i.test(req.headers['user-agent']);
    },
    iOS: function() {
        return /iPhone|iPad|iPod/i.test(req.headers['user-agent']);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
  };

  if(isMobile.Android()){
    res.redirect('https://play.google.com/store/apps/details?id=info.meltemsahin.PMS');
  }else if(isMobile.iOS()){
    res.redirect('https://itunes.apple.com/tr/app/pms-augmented-reality/id1224593000');
  }else{
    res.redirect('https://ohmypms.com');
  }

})

router.post('/contact', function(req, res, next) {

  if (!req.body.message || !req.body.email) {
    return res.json({response: 'Fill the form please!'});
  }

  var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
  var request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: {
      personalizations: [
        {
          to: [
            {
              email: 'erdincakkaya@gmail.com'
            },
            {
              email: 'meeltemsahin@gmail.com'
            }
          ],
          subject: '[OH MY PMS] New Message from ' + req.body.name
        }
      ],
      from: {
        email: req.body.email || 'unknown sender'
      },
      content: [
        {
          type: 'text/plain',
          value: req.body.message || 'empty message'
        }
      ]
    }
  });

  sg.API(request, function (error, response) {
    if (error) {
      console.log('Error response received');
    }
    res.json({"response": "Thank you! We will contact you soon!"});
  });

});

module.exports = router;
