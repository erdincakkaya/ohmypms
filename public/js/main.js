
$(document).ready(function(){

  document.getElementById('pmshand').play();

  if($('video').height() < 541){
    $('#actionSection').css('height', $('video').width()/2.37);
  }

  $("#contact-submit").click(function(e){
    e.preventDefault();
    $.post("/contact", $("#contact-form").serialize(), function(data) {
        alert(data.response);
        if (data.response !== 'Fill the form please!') {
          $('#contact-form')[0].reset();
        }
    });
  });
});

$( window ).resize(function() {
  if($('video').height() < 541){
    $('#actionSection').css('height', $('video').height());
  }
});

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-98333456-1', 'auto');
ga('send', 'pageview');
